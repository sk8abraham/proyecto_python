#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
#Integrantes:
#Manzano Cruz Isaias Abraham
#Martinez Salazar Luis Antonio
#Leal Gonzalez Ignacio


import sys
import optparse
import ConfigParser
import httplib
import ssl
import OpenSSL
from BeautifulSoup import BeautifulSoup
from time import sleep
from requests import get
from requests import put
from requests import options
from requests import post
from requests import delete
from requests import head
from requests import patch
from requests import session
from requests.exceptions import ConnectionError
from re import search
from datetime import datetime
from random import choice

def addOptions():
    '''
    Funcion que parsea los datos que se tomen de linea de comandos como opciones para ejecutar el programa
    Devuelve un objeto cuyos atributos son las opciones de ejecucion
    '''
    parser = optparse.OptionParser()
    parser.add_option('-u','--url', dest='url', default=None, help='Indicates the url to perform the attack, format: {http|https}://{ip|domainname}[/directory]:port')
    parser.add_option('-v', '--verbose', action='store_true',dest='verbose', default=False, help='If activated, the script will show detailed information of what is happening in every step, if not the script will run in silent mode')
    parser.add_option('-r', '--results', dest='results', default=None, help='Indicates the name of the file where the results will be written')
    parser.add_option('-m', '--methods', action='store_true', dest='methods', default=False, help='If activated the script will show the enabled methods in the server')
    parser.add_option('-i', '--infile', dest='infile', default=None, help='Indicates the file which contains the name of the files to search')
    parser.add_option('-o', '--output', dest='output', default=None, help='Indicates the name of file where the report will be created')
    parser.add_option('-s', '--ssl_tls', action='store_true', dest='ssl_tls', default=False, help='If activated the script will get information about the SSL/TLS certificate of the server')
    parser.add_option('-U', '--useragent', dest='useragent', default=None, help='Indicates the User-Agent to use in each request')
    parser.add_option('-e', '--extractv',action='store_true',dest='extractv', default=None, help='If activated the script will extract the versions of the server (HTTP header and HTTP label)')
    parser.add_option('-t', '--time', dest='time', default=None, help='Indicates the time in seconds between requests to avoid blocks from the server')
    parser.add_option('-c', '--config', dest='config', default=None, help='Indicates the name of the file which contains all options here described')
    opts,args = parser.parse_args()
    return opts


def addOptionsFile(configfile):
    '''
    Funcion que parsea los datos que se tomen de un archivo como opciones para ejecutar el programa
    Recibe el nombre del archivo del que se obtendrán las opciones de ejecucion
    Devuelve un objeto cuyos atributos son las opciones de ejecucion
    '''
    config = ConfigParser.ConfigParser()
    config.read(configfile)
    parser = optparse.OptionParser()
    parser.add_option('-u','--url', dest='url', default=config.get("Options","url"), help='Indicates the url to perform the attack, format: {http|https}://{ip|domainname}:port[/directory]/')
    parser.add_option('-v', '--verbose', action='store_true',dest='verbose', default=str2bool(config.get("Options","verbose")), help='If activated, the script will show detailed information of what is happening in every step, if not the script will run in silent mode')
    parser.add_option('-r', '--results', dest='results', default=config.get("Options","results"), help='Indicates the name of the file where the results will be written')
    parser.add_option('-m', '--methods', action='store_true', dest='methods', default=str2bool(config.get("Options","methods")), help='If activated the script will show the enabled methods in the server')
    parser.add_option('-i', '--infile', dest='infile', default=config.get("Options","infile"), help='Indicates the file which contains the name of the files to search')
    parser.add_option('-o', '--output', dest='output', default=config.get("Options","output"), help='Indicates the name of file where the report will be created')
    parser.add_option('-s', '--ssl_tls', action='store_true', dest='ssl_tls', default=str2bool(config.get("Options","ssl_tls")), help='If activated the script will get information about the SSL/TLS certificate of the server')
    parser.add_option('-U', '--useragent', dest='useragent', default=config.get("Options","useragent"), help='Indicates the User-Agent to use in each request')
    parser.add_option('-e', '--extractv',action='store_true',dest='extractv', default=str2bool(config.get("Options","extractv")), help='If activated the script will extract the versions of the server (HTTP header and HTTP label)')
    parser.add_option('-t', '--time', dest='time', default=int(config.get("Options","time")), help='Indicates the time in seconds between requests to avoid blocks from the server')
    parser.add_option('-c', '--config', dest='config', default=None, help='Indicates the name of the file which contains all options here described')
    opts,args = parser.parse_args()
    return opts


def print_verbose(message,verbose):
    '''
    Funcion que recibe un mensaje como parametro, el cual dira que es lo que esta haciendo.
    '''
    if verbose and len(message)>1:
        print message
        
def str2bool(v):
    '''
    Funcion que convierte una cadena en un tipo de dato booleano
    Recibe una cadena
    Devuelve un booleano
    '''
    return v.lower() == 'true'
    
    
def checkOptions(options):
    '''
    Funcion que verifica las opciones minimas para que el programa pueda correr correctamente, en caso de no cumplir con los requerimientos minimos
    el programa termina su ejecucion
    Recibe un objeto con las opciones de ejecucion del programa
    '''
    if options.url is None:
        printError('You must specify a server to atack.', True)
    elif options.infile is None:
        printError('You must specify the file wich has the name of the files to search',True)

 
def verify_url(url):
    '''
    Funcion que verifica los argumentos minimos para poder ejecutar el programa
    '''
    http_re=r"(http://.*:[0-9]{2}(/.*)?/$)"
    https_re=r"(https://.*:[0-9]{3}(/.*)?/$)"
    if search(http_re,url):
        return 'http'
    elif search(https_re,url):
        return 'https'
    else:
        printError('Invalid URL :%s'%url,True)
      

def printError(msg, exit = False):

    '''
    Esta funcion imprime en la salida de error estandar un mensaje
    Recibe:	
	msg:	mensaje a imprimir y exit:  exit el cual indica si el el programa termina su ejecucion o no
	exit:	Si es True termina la ejecucion del programa
    '''

    sys.stderr.write('Error:\t%s\n' % msg)
    if exit:
        sys.exit(1)



def create_results(webpage, results_file):
    '''
    Esta funcion crea el archivo de resultados, si no recibe un nombre de archivo para los resultados creara uno llamado "results.txt"
    Recibe:	
	webpage: nombre de la pagina para indicar el origen de lo analizado
    '''

    if results_file == None or results_file == '':
        print 'A file named "results.txt" will be created with the results'
        with open('results.txt','w') as f_results:
            f_results.write(' Results File '.center(70,'=')+'\n\n')
            f_results.write('Analized server: ' + webpage+'\n')
    else:
        print 'A file named "%s" will be created with the results'% results_file
        with open(results_file,'w') as f_results:
            f_results.write(' Results File '.center(70,'=')+'\n\n')
            f_results.write('Analized server: ' + webpage)


def print_results(message, results_file):
    '''
    Esta funcion escribe un mensaje en el archivo de resultaos del programa, se escribira al final del archivo
    en caso de que ya exista el archivo "results.txt"
    Recibe:	
    	message: el mensaje a imprimir en el archivo de resultados
    '''

    if results_file == None or results_file == '':
        with open('results.txt','a') as f_results:
            f_results.write(message+'\n')
    else:
        with open(results_file,'a') as f_results:
            f_results.write(message+'\n')
            

def create_report(webpage, options, report_file):
    '''
    Esta funcion crea un archivo de reporte, si no recibe un nombre de archivo para el reporte
    creara uno llamado "report.txt"
    '''
    if report_file == None or report_file == '':
        with open('report.txt','w') as f_report:
            print 'A file named "report.txt" will be created with a report of the script"'
            f_report.write(' Report File '.center(70,'=')+'\n\n')
            f_report.write('Runtime: ' + str(datetime.now()) + '\n' + 'Analized server: ' + webpage + '\n' + 'Execution options: ' + str(options)[1:-1].replace("'",'') + '\n')
    else:
        print 'A file named "%s" will be created with a report of the script'% report_file
        with open(report_file,'w') as f_report:
            f_report.write(' Report File '.center(70,'=')+'\n\n')
            f_report.write('Runtime: ' + str(datetime.now()) + '\n' + 'Analized server: ' + webpage + '\n' + 'Execution options: ' + str(options)[1:-1].replace("'",'') + '\n')


def print_report(message, report_file):
    '''
    Esta funcion escribe un mensaje en el archivo de reporte del programa, se escribira al final del archivo
    en caso de que ya exista el archivo "report.txt"
    Recibe:	
    	message: el mensaje a imprimir en el archivo de resultados
    '''

    if report_file == None or report_file == '':
        with open('report.txt','a') as f_report:
            f_report.write(message+'\n')
    else:
        with open(report_file,'a') as f_report:
            f_report.write(message+'\n')


def metodos_http(url):
    '''
    Esta funcion se encarga de revisar los metodos http habilitados en un servidor
    Recibe:
    	url:	servidor a analizar
    Regresa:
    	salida:	cadena con informacion de los metodos habilitados
    '''

    salida="Metodos http que contiene la direccion:\n"
    if put(url).status_code == 200:
        salida+= "Tiene metodo put\n"
    if get(url).status_code == 200:
        salida+= "Tiene metodo get\n"
    if options(url).status_code == 200:
        salida+= "Tiene metodo options\n"
    if post(url).status_code == 200:
        salida+= "Tiene metodo post\n"
    if delete(url).status_code == 200:
        salida+= "Tiene metodo delete\n"
    if head(url).status_code == 200:
        salida+= "Tiene metodo head\n"
    if patch(url).status_code == 200:
        salida+= "Tiene metodo patch\n"
    return salida


def informacion(url):
    '''
    Funcion que recibe como parametro la url y obtiene a traves de los encabezados HTTP informacion del servidor, que tipo de servidor es 'Server' y el 'X-Powered-By' y en caso de utilizar csm muestra
    cual utiliza, devolviendo el servidor el x-powered-by y el csm, los cuales se escribiran en el reporte.
    '''
    server = ''
    powered = ''
    cms = ''
    texto1 = "Version del servidor: "
    texto2 = "X-Powered-By: "
    texto3 = "CSM: "
    r = get(url)
    cabeceras = r.headers
    html = r.content
    parsed_html = BeautifulSoup(html)
    if 'Server' in cabeceras:
        server = cabeceras['Server']
        server = texto1 + server
    if 'X-Powered-By' in cabeceras:
        powered = cabeceras['X-Powered-By']
        powered = texto2 + powered
    for var in parsed_html.findAll('meta'):
        var2 = var.get('name')
        if var2 == 'generator':
            cms = var.get('content')
            cms = texto3 + csm
    return server, powered, cms


def certificados(url):
    '''
    Funcion que recibe como parametro la url y obtiene informacion del certificado, si es que utiliza https, regresando la informacion del certificado la cual se escribira en el archivo.
    '''
    texto = "Informacion del certificado: "
    url2=url.split(':')[1][2:]
    cert = ssl.get_server_certificate((url2, 443))
    x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
    informacion = str(x509.get_subject().get_components())
    informacion = texto + informacion
    try:
        req=get(url,verify=True)
        message = 'El servidor tiene un certificado valido'
    except:
        message = 'El servdor tiene un certificado no valido'
    return informacion,message


def hora():
    '''
    Funcion que devuelve la hora en que se esta ejecutando el script.
    '''
    return str(datetime.now())


def make_agent(agent):
    '''
    Funcion que devuelve una lista, con los agentes a utiliar en cada peticion
    Recibe un archivo o una opcion dada por la linea de comandos
    Devuelve una lista con el/los agente(s)
    '''
    try:
        return open(agent).read().splitlines()
    except:
        return [agent]

def make_requests(url, verbose, files, user_agent, extractv, methods, ssl_tls, protocol, results, report, time=2):
    '''
	Funcion que hace las peticiones al sitio web que se quiere atacar
	Recibe la url, si se desea imprimir mensajes con verbose, el archivo de de donde va a sacar los archivos a buscar
	el user-agent que el programa utilizara, extractv que indica si se va a obtener informacion del html, methods para 
	indicar si se va a obtener los metdos que tiene habilitado el sitio, ssl_tls para obtener informacion de los certificados
	protocol para validar que si se va a obtener informacion de los certificados sea una pagina por https, results que indica
	el archivo donde se escribiran los resultados, report que es el archivo de reporte y el tiempo de espera entre cada peticion
	'''
    cont=0
    try:
        if methods:
            message1 = metodos_http(url)
            print_verbose(message1,verbose)
            print_report(message1,report)
        if ssl_tls and protocol == 'https':
            info,certs = certificados(url)
            print_verbose(info,verbose)
            print_verbose(certs,verbose)
            print_report(info,report)
            print_report(certs,report)
        if extractv:
            server,powered,cms=informacion(url)
            print_verbose(server,verbose)
            print_verbose(powered,verbose)
            print_verbose(cms,verbose)
            print_report(server,report)
            print_report(powered,report)
            print_report(cms,report)
    	with open(files, 'r') as f_files:
            for fl in f_files:
                fl = fl.strip('\n')
                url_file=url+fl
                s=session()
                headers={}
                headers['User-agent']=choice(user_agent)
                response=s.get(url_file,headers=headers)
                if response.status_code == 200:
                    leng = len(response.content)
                    message='File found: %s, size: %d' %(fl,leng)
                    print_verbose(message, verbose)
                    print_report(message, report)
                    cont+=1
        	else:
                    print_verbose('File not found: %s, code: %d'%(fl,response.status_code), verbose)
            	sleep(time)
    except ConnectionError:
        printError('Error en la conexion, tal vez el servidor no esta arriba.',True)
    finally:
        print_results('Se encontraron: %d archivos en el servidor'%cont,results)


if __name__ == '__main__':
    try:
        opts = addOptions()
        if opts.config != None:
            opts = addOptionsFile(opts.config)
        checkOptions(opts)
        protocol = verify_url(opts.url)
        create_results(opts.url, opts.results)
        create_report(opts.url, opts, opts.output)
        if opts.useragent == None or opts.useragent=='':
            user_agent=make_agent('user_agents.txt')
        else:
            user_agent=make_agent(opts.useragent)        
        make_requests(opts.url, opts.verbose, opts.infile, user_agent, opts.extractv, opts.methods, opts.ssl_tls, protocol, opts.results, opts.output, opts.time)
    except Exception as e:
        printError('An unexpected error happend :(')
        printError(e, True)